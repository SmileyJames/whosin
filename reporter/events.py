import requests
from datetime import datetime
from bs4 import BeautifulSoup
from urlparse import urljoin
import re
from django.templatetags.static import static
from icalendar import Calendar

class Event(object):
    team = []
    is_setup = False

    def __init__(self, ical_event):
        if not self.is_setup:
            self.setup()
        self.event = ical_event
        self.handle_start_date()
        self.handle_end_date()
        self.handle_summary()
        self.handle_description()
        self.handle_status()

    @classmethod
    def setup(self):
        rkh_url = "https://www.rkh.co.uk/about-us"
        text = requests.get(rkh_url).text
        html = BeautifulSoup(text)
        team_list = html.find(id = "team_list")
        team_members = team_list.find_all("article", {"class": "col"})
        for member in team_members:
            name = member.find("h3").text
            image = urljoin(rkh_url, member.find("img")["src"])
            image = re.findall(r"^(.+\.png)\..+$", image)[0]
            self.team.append((name, image))
        self.is_setup = True

    def handle_start_date(self):
        self.start_date = self.event.decoded("dtstart")
        self.start_date = datetime(self.start_date.year, self.start_date.month, self.start_date.day)

    def handle_end_date(self):
        self.end_date = self.event.decoded("dtend")
        self.end_date = datetime(self.end_date.year, self.end_date.month, self.end_date.day)

    def handle_summary(self):
        string = self.event["summary"]
        self.employee = ""
        self.image = static("images/PIGION.png")
        for name, image in self.team:
            if name in string:
                self.employee = name
                self.employee_first_name = name.split()[0]
                self.image = image

        try:
            if self.employee:
                self.summary = re.findall(r"^.+-\ (.*)$", string)[0]
            else:
                self.summary = string
        except IndexError:
            self.summary = string or ""

    def handle_description(self):
        self.description = self.event["description"] or ""

    def handle_status(self):
        self.status = self.event["status"] or ""


def parse(url):
    text = requests.get(url).text
    calendar = Calendar.from_ical(text)
    return [Event(event) for event in calendar.subcomponents]
