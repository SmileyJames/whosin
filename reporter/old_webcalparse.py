import requests
from datetime import datetime, date
from bs4 import BeautifulSoup
from urlparse import urljoin
import re
from django.templatetags.static import static

class Event(object):
    attributes = {
                    "DESCRIPTION:": "handle_description",
                    "DTSTART;VALUE=DATE:": "handle_start_date",
                    "DTSTART:": "handle_start_date",
                    "DTEND:": "handle_end_date",
                    "DTEND;VALUE=DATE:": "handle_end_date",
                    "SUMMARY:": "handle_summary",
                    "STATUS:": "handle_status"
                 }
    team = []

    @classmethod
    def setup(self):
        pass
        #"""
        rkh_url = "https://www.rkh.co.uk/about-us"
        text = requests.get(rkh_url).text
        html = BeautifulSoup(text)
        team_list = html.find(id = "team_list")
        team_members = team_list.find_all("article", {"class": "col"})
        for member in team_members:
            name = member.find("h3").text
            image = urljoin(rkh_url, member.find("img")["src"])
            image = re.findall(r"^(.+\.png)\..+$", image)[0]
            self.team.append((name, image))
        #"""

    def parse(self, line):
        for key, value in self.attributes.items():
            if line.startswith(key):
                data = line[len(key):]
                getattr(self, value)(data)

    def handle_start_date(self, string):
        self.start_date = None
        year = int(string[0:4])
        month = int(string[4:6])
        day = int(string[6:8])
        self.start_date = date(year, month, day)

    def handle_end_date(self, string):
        self.end_date = None
        year = int(string[0:4])
        month = int(string[4:6])
        day = int(string[6:8])
        self.end_date = date(year, month, day)

    def handle_summary(self, string):
        self.employee = ""
        self.image = static("images/PIGION.png")
        for name, image in self.team:
            if name in string:
                self.employee = name
                self.employee_first_name = name.split()[0]
                self.image = image

        try:
            if self.employee:
                self.summary = re.findall(r"^.+-\ (.*)$", string)[0]
            else:
                self.summary = string
        except IndexError:
            self.summary = string or ""

    def handle_description(self, string):
        self.description = string or ""

    def handle_status(self, string):
        self.status = string

def parse(url):
    Event.setup()
    text = requests.get(url).text
    current_event = None
    events = []
    for line in text.splitlines():
        if line.startswith("BEGIN:VEVENT"):
            current_event = Event()
        elif line.startswith("END:VEVENT") and current_event:
            yield current_event
            current_event = None
        elif current_event:
            current_event.parse(line)
