from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<day>[0-9]{2})\-(?P<month>[0-9]{2})\-(?P<year>[0-9]{4})/$', views.index, name='date'),
]
