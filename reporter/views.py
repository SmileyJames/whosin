from django.shortcuts import render
from .events import parse
from datetime import datetime, timedelta

feed_url = "https://app.timetastic.co.uk/Feeds/OrganisationCalendar/29345133-6108-4fc6-a254-45eadaf7a67c"

def index(request, day = None, month = None, year = None):
    if day and month and year:
        date = datetime(int(year), int(month), int(day))
        date_string = date.strftime("%d %B %Y")
    else:
        today = datetime.today()
        date = datetime(today.year, today.month, today.day)
        date_string = "today"
    start_date = date
    end_date = date + timedelta(days=1)
    events = parse(feed_url)
    events = filter(lambda event: event.start_date < end_date and event.end_date > start_date, events)
    return render(request, "index.html", {"holidays": events, "date": date_string})
